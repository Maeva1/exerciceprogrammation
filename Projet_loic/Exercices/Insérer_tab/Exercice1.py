import random

def recherche(liste_ordonnee: list, nombre: int):
    """
            Cette fonction recherche la bonne position

            Parameters
            ----------
            liste ordonée
            nombre à insérer

            Returns
            -------
            nombre : la position du nombre à insérer


    """
    if len(liste_ordonnee) == 1:
        return 0
    milieu = len(liste_ordonnee)//2
    

    if liste_ordonnee[milieu] == nombre:
        return milieu
    elif liste_ordonnee[milieu] > nombre:
        return recherche(liste_ordonnee[:milieu], nombre)
    else:
        return milieu + recherche(liste_ordonnee[milieu:], nombre)


def insertion(liste: list, position: int, elem_inser: int):
    """
         Cette fonction insere un nombre dans une liste à une position donné

        Parameters
        ----------
        liste ordonée
        nombre à insérer
        position

        Returns
         -------
        liste : liste avec le nombre inséré à la bonne place


     """
    if position == 1 and liste[0] > elem_inser:
        position = 0
        liste.insert(position, elem_inser)
    else:
        liste.insert(position, elem_inser)
    print("Liste triée : ", liste)



random_list = [random.randint(0,20) for i in range(10)]
random_list.sort()
random_number = random.randint(0, 20)
print("Liste entrée :",random_list,"nombre inséré :", random_number)
v = recherche(random_list, random_number) + 1 
insertion(random_list, v, random_number)
